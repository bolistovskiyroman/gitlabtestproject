# frozen_string_literal: true

def get_moves
  moves = %i[left right up down]
  # moves.sample
  x = rand(0..3)
  moves[x]
end

command = get_moves
puts "Recieve command: #{command}"
if command == :left
  puts 'Robot moves left'
elsif command == :right
  puts 'Robot moves right'
elsif command == :up
  puts 'Robot moves up'
elsif command == :down
  puts 'Robot moves down'
end
